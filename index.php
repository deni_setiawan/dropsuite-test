<?php
set_time_limit(0);
ini_set("memory_limit", -1);

require_once("Dropsuite.php");

/**
* @author Deni Setiawan
*
* this file is to get the bigger number of file that has the same content
*
* How To Run:
* use following command "php index.php [folder to scan] [1/0]"
*
*/

$targetDir = (sizeof($argv) > 1) ? $argv[1] : die("Please input directory which going to process.\n");
$debug = (isset($argv[2])) ? $argv[2] : 0;

print "Starting Process... Please wait..." . PHP_EOL;
$ds = new Dropsuite($debug);
$ds->giveMeTheBiggestNumberOfFiles($targetDir);
