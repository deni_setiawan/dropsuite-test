<?php
/**
* Simple class for the main process
*
*/
class Dropsuite
{
	protected $debug;

	public function __construct($debug = false)
    { 
    	$this->debug = $debug;
    }

    /**
    * get all the folders available and keep them into array
    * @param targetDir
    * @param result
    */
    function walkTheFolders($targetDir, &$result = array()) 
	{
		($this->debug)? print "walkTheFolders " . $targetDir . "..." . PHP_EOL : "";
		$files = scandir($targetDir);
		$files = array_diff($files, array(".","..",".DS_Store"));
		foreach($files as $file) {
			$filePath = realpath($targetDir . DIRECTORY_SEPARATOR . $file);
			if (is_dir($filePath)) {
				$this->walkTheFolders($filePath, $result);
			} else {
				$result[] = $filePath;
			}
			sleep(0.3);
		}
		return $result;
	}

	/**
	* read file content, use SplFileObject to handle bigger file size
	* @param path
	*/
	function iterateText($path)
	{
		($this->debug)? print "iterateText " . $path . " ..." . PHP_EOL : "";
		$file = new SplFileObject($path, "r");
	    while (!$file->eof()) {
	        yield $file->fgets();
	        sleep(0.1);
	    }
	}

	/**
	* core function, which use to compare and count files
	* @param targetDir
	*/
	function giveMeTheBiggestNumberOfFiles($targetDir)
	{
		$allfiles = $this->walkTheFolders($targetDir);
		if (sizeof($allfiles) == 0) {
			die("There is no file to process... Exit." . PHP_EOL);
		}
		$content_helper = array();
		$hashed_count = array();
		$realdata = array();
		foreach ($allfiles as $file) {
			if (!$file) {
				continue;
			}

		    $text = "";
		    $iterator = $this->iterateText($file);

		    foreach ($iterator as $line) {
		        $text .= $line;
		    }

		    if (empty($text)) {
		        continue;
		    }

		    $hashed_text = md5($text);
		    if (in_array($hashed_text, $content_helper)) {
		        $x = $hashed_count[$hashed_text] + 1;
		        $hashed_count[$hashed_text] = $x;
		        $realdata[$hashed_text] = base64_encode($text);
		    } else {
		        $content_helper[] = $hashed_text;
		        $hashed_count[$hashed_text] = 1;
		        $realdata[$hashed_text] = base64_encode($text);
		    }
		    sleep(0.3);
		}
		arsort($hashed_count);
		$output = array_slice($hashed_count, 0, 1);
		$output_hash = array_keys($output);
		$output_count = array_values($output);
		print "Final Result: " . base64_decode($realdata[$output_hash[0]]) . " " . $output_count[0];
		print PHP_EOL;
	}
}